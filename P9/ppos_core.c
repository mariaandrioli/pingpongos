// Maria Teresa Kravetz Andrioli GRR20171602
// Ana Carolina Faria Magnoni GRR20166808
#include "ppos.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/time.h>

#define STACKSIZE 32768		/* tamanho de pilha das threads */

task_t Main, *Current, Dispatcher;
task_t *ready, *sleeping;
int id, contaTicks;
int totalTasks = 0;
int usando_prio = 0;
unsigned int tempo;
unsigned int t1=0, t2=0;

// estrutura que define um tratador de sinal (deve ser global ou static)
struct sigaction action ;

// estrutura de inicialização to timer
struct itimerval timer;

unsigned int systime(){
	return(tempo);
}

void print_elem (void *ptr)
{
   task_t *elem = ptr ;

   if (!elem)
      return ;

   elem->prev ? printf ("%d", elem->prev->id) : printf ("*") ;
   printf ("<%d>", elem->id) ;
   elem->next ? printf ("%d", elem->next->id) : printf ("*") ;
}

void dispatcherBody();
task_t *scheduler();

void tratador (int signum){
	if(!Current->sis_task){
		if(contaTicks > 0){
			contaTicks--;
		}
		else{
			task_yield();
			Current->tempo_process += (systime() - t2);
		}
	}
	tempo++;
}

void ppos_init () {
	/* desativa o buffer da saida padrao (stdout), usado pela função printf */
	setvbuf (stdout, 0, _IONBF, 0) ;
	contaTicks = 20;
	Main.prev = NULL;
	Main.next = NULL;
	Main.id = 0;
	Main.ativos = 1;
	Main.status = 0;
	Main.sis_task = 0;
	Main.tempo_exec = systime();
	Main.tempo_process = 0;
	Current = &Main;

	task_create(&Dispatcher, dispatcherBody, NULL);
	queue_remove((queue_t **)&ready, (queue_t *) ready);
	queue_append((queue_t **) &ready, (queue_t *) &Main);
	
	// queue_print ("Saida gerada  ", (queue_t*) ready, print_elem) ;
	
	Dispatcher.sis_task = 1;
	Dispatcher.status = 0;

	action.sa_handler = tratador;
	sigemptyset(&action.sa_mask);
	action.sa_flags = 0;
	if (sigaction (SIGALRM, &action, 0) < 0)
	{
	  perror ("Erro em sigaction: ");
	  exit (1);
	}

	// ajusta valores do temporizador
	timer.it_value.tv_usec = 1000;      // primeiro disparo, em micro-segundos
	timer.it_value.tv_sec  = 0;      // primeiro disparo, em segundos
	timer.it_interval.tv_usec = 1000;   // disparos subsequentes, em micro-segundos
	timer.it_interval.tv_sec  = 0;   // disparos subsequentes, em segundos
	
	// arma o temporizador ITIMER_REAL (vide man setitimer)
	if (setitimer (ITIMER_REAL, &timer, 0) < 0)
	{
	perror ("Erro em setitimer: ");
	exit (1);
	}
}

int task_create (task_t *task, void (*start_func)(void *), void *arg) {
	char *stack;

	stack = malloc(STACKSIZE);

	getcontext (&task->context) ;

	stack = malloc (STACKSIZE) ;
	if (stack)
	{
		task->context.uc_stack.ss_sp = stack ;
		task->context.uc_stack.ss_size = STACKSIZE ;
		task->context.uc_stack.ss_flags = 0 ;
		task->context.uc_link = 0 ;
		makecontext (&task->context, (void*) (*start_func), 1, arg) ;
		task->id = ++id;
		if(task != &Dispatcher){
			task->sis_task = 0;
		}
		task->tempo_exec = systime();
		task->tempo_process = 0;
		task->ativos = 1;
		task->status = 0;

		queue_append((queue_t **) &ready, (queue_t *) task);
		
		#ifdef DEBUG
		printf ("task_create: criou tarefa %d\n", task->id) ;
		//queue_print ("Saida gerada  ", (queue_t*) task, print_elem) ;
		#endif
	}
	else
	{
		perror ("Erro na criação da pilha: ") ;
		exit (1) ;
	}
	return(task->id);
}

void task_exit (int exitCode) {
	// #ifdef DEBUG
	// #endif
	Current->tempo_exec = (systime()-Current->tempo_exec);
	printf("Task %d exit: execution time %d ms, processor time %d ms, %d activations\n", task_id(), Current->tempo_exec, Current->tempo_process, Current->ativos);
	if (Current->id == 1)
		task_switch(&Main);
	else {
		Current->status = 1;
		task_t *aux = Current->dependQueue;
		while(Current->dependQueue){
			aux->status = 0;
			aux->exitCode = exitCode;
			queue_append((queue_t **) &ready, ((queue_t *)(queue_remove((queue_t **)&Current->dependQueue, (queue_t *) aux))));
			aux = aux->next;
		}
		queue_remove((queue_t **)&ready, (queue_t *) Current);
		task_switch(&Dispatcher);
	}
}

int task_switch (task_t *task) {
	task_t *aux;

	aux = Current;
	Current = task;
	task->ativos++;

	#ifdef DEBUG
	printf ("task_switch: trocando contexto %d -> %d\n", aux->id, task->id) ;
	#endif
	swapcontext(&aux->context, &task->context);

	if (errno >= 0)
		return(-1);

	return(0);
}

int task_id () {
	return (Current->id);
}

void task_yield () {
    #ifdef DEBUG
	printf("task_yield: a tarefa %d pediu yield\n", Current->id);
    #endif
    task_switch(&Dispatcher);
}

task_t *scheduler(){

	task_t *prox = ready;

    if((usando_prio) && (ready)){
	    task_t *aux = ready->next;
	    while(aux != ready)
	    {
	        if(aux->din_prio < prox->din_prio)
	            prox = aux;
	        aux = aux->next;
	    }

	    aux = ready->next;
	    
	    while(aux != ready)
	    {
	        if(aux->din_prio > -20){
	            aux->din_prio--;
	            #ifdef DEBUG
	            printf("scheduler: a tarefa %d tem prio_din %d\n", aux->id, aux->din_prio);
	            #endif
	        }
	        aux = aux->next;
	    }
	    prox->din_prio = prox->est_prio;
    }

	if(ready) 
		ready = ready->next;
		

    return(prox);
}

void dispatcherBody() { // dispatcher é uma tarefa 
	task_t *next;



	while ((ready) || (sleeping))
	{
		next = scheduler() ;  // scheduler é uma função
		if ((next) && (!next->status))
		{
	  		contaTicks = 20;
	  		t2 = t1;
	  		t1 = systime();
	    	task_switch(next) ; // transfere controle para a tarefa "next"
	    }	
	    if (sleeping){
			task_t *aux = sleeping->next;
			while (aux != sleeping)
		    {			
		        printf("%d %d %d\n", aux->id, systime(), aux->time);
		        if(systime() > aux->time){
		        	aux->status = 0;
		        	queue_append((queue_t **) &ready, ((queue_t *)(queue_remove((queue_t **)&sleeping, (queue_t *) aux))));
					// queue_print ("Saida gerada  ", (queue_t*) sleeping, print_elem) ;
		        }
		        aux = aux->next;
		    }
		}
    }
	task_exit(0) ; // encerra a tarefa dispatcher
}

void task_setprio (task_t *task, int prio) {
	usando_prio = 1;
	if(task == NULL){
		Current->est_prio = prio;
		Current->din_prio = prio;
	}
	else{
		task->est_prio = prio;
		task->din_prio = prio;
	}
}

int task_getprio (task_t *task) {
	if(task == NULL)
		return(Current->est_prio);
	else
		return(task->est_prio);

}

int task_join (task_t *task){
	if(task == NULL){
		return(-1);
	}
	Current->status = 2;
	queue_append((queue_t **) &task->dependQueue, ((queue_t *)(queue_remove((queue_t **)&ready, (queue_t *) Current))));
	task_switch(&Dispatcher);
	return(Current->exitCode);
}

void task_sleep (int t) {
	Current->status = 2;
	Current->time = systime() + t*1000;

	queue_append((queue_t **)&sleeping, ((queue_t *)(queue_remove((queue_t **)&ready, (queue_t *) Current))));

			queue_print ("Fila dormindo  ", (queue_t*) sleeping, print_elem) ;
	task_switch(&Dispatcher);
}