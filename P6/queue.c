// Ana Carolina Faria Magnoni GRR20166808
// Maria Teresa Kravetz Andrioli GRR20171602

#include "queue.h"
#include <stdlib.h>
#include <stdio.h>

int exists(queue_t **queue, queue_t *elem){
    queue_t *pointer = *queue;
    if (!pointer)
        return 0;
    if (pointer == elem)
        return 1;
    pointer = pointer->next;
    while (pointer != *queue)
    {
        if (pointer == elem)
            return 1;
        pointer = pointer->next;
    }
    return 0;
}

void queue_append (queue_t **queue, queue_t *elem){
		
	if (!elem){
		fprintf(stderr, "O elemento a ser inserido na fila nao existe.");
	}
	else if(sizeof(**queue) != sizeof(queue_t)){
		fprintf(stderr, "A fila nao existe.");
	}
	else if((elem->next != NULL) || (elem->prev != NULL)){
		fprintf(stderr, "O elemento pertence a outra fila e, por isso, nao pode ser inserido na fila desejada.");
	}
	else{
		if(!*queue){
			*queue = elem;
			(*queue)->next = *queue;
			(*queue)->prev = *queue;
			return;
		}
		elem->prev = (*queue)->prev;
	    elem->next = (*queue);
	    (*queue)->prev->next = elem;
	    (*queue)->prev = elem;
	}

	return;
}

queue_t *queue_remove (queue_t **queue, queue_t *elem){
	
	if (!elem){
		fprintf(stderr, "O elemento a ser removido da fila nao existe.");
		return(NULL);
	}
	else if(sizeof(**queue) != sizeof(queue_t)){
		fprintf(stderr, "A fila nao existe.");
		return(NULL);
	}
	else if(!*queue){
		fprintf(stderr, "A fila esta vazia e, portanto, nao ha elementos a remover.");
		return(NULL);
	}
	if(!exists(queue, elem)){
		fprintf(stderr, "O elemento nao esta na fila indicada e, por isso, nao pode ser removido.");
		return(NULL);
	}
	else{
		if(*queue == elem){
			if(elem->next == elem)
				*queue = NULL;
			else{
				*queue = elem->next;
				elem->prev->next = elem->next;
				elem->next->prev = elem->prev;
			}
		}
		else{
			elem->prev->next = elem->next;
			elem->next->prev = elem->prev;
		}
		elem->next = NULL;
		elem->prev = NULL;
		return(elem);
	}

	return(NULL);
}

int queue_size (queue_t *queue){

	int q = 0;
	queue_t *aux;

	if(!queue){
		return(q);
	}

	aux = queue;
	q++;
	while(aux->next != queue){
		q++;
		aux = aux->next;
	}
	return(q);
}

void queue_print (char *name, queue_t *queue, void print_elem (void*) ){
	
	printf("%s", name);
	printf("[");

	if(!queue){
		printf("]\n");
		return;
	}	
	
	queue_t *aux = queue;
	do{
		if(aux->prev->next != queue)
        	printf(" ");
        print_elem(aux);
        aux = aux->next;
	}while(aux != queue);
    printf("]\n");
}
