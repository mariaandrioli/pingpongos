// PingPongOS - PingPong Operating System
// Prof. Carlos A. Maziero, DINF UFPR
// Versão 1.1 -- Julho de 2016

// Estruturas de dados internas do sistema operacional

#ifndef __PPOS_DATA__
#define __PPOS_DATA__

#include <ucontext.h>		// biblioteca POSIX de trocas de contexto
#include "queue.h"		// biblioteca de filas genéricas
#include <string.h>

// Estrutura que define um Task Control Block (TCB)
typedef struct task_t
{
   struct task_t *prev, *next ;		// ponteiros para usar em filas
   int id ;				// identificador da tarefa
   ucontext_t context ;			// contexto armazenado da tarefa
   void *stack ;			// aponta para a pilha da tarefa
   int din_prio;
   int est_prio;
   int sis_task;
   int vezes_exec;
   unsigned int tempo_exec;
   unsigned int tempo_process;
   int ativos;
   int status; //se terminada = 1, se rodando = 0, se suspensa = 2
   struct task_t *dependQueue;
   int exitCode;
   int time;
   int canPreempt;
} task_t ;

// estrutura que define um semáforo
typedef struct
{
  int cont;
  int valid;
  task_t *wait_list;
} semaphore_t ;

// estrutura que define um mutex
typedef struct
{
  // preencher quando necessário
} mutex_t ;

// estrutura que define uma barreira
typedef struct
{
  // preencher quando necessário
} barrier_t ;

// estrutura que define uma fila de mensagens
typedef struct
{
  void *msgs;
  int inicio, fim, num_msgs;
  int max_msgs, msg_size, isValid;
  semaphore_t s_item, s_vaga, s_buffer;
} mqueue_t ;

#endif

