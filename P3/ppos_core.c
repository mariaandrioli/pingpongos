// Maria Teresa Kravetz Andrioli GRR20171602
// Ana Carolina Faria Magnoni GRR20166808
#include "ppos.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

task_t Main, *Current, Dispatcher;
int id;
int userTasks = 0;
task_t *ready;

#define STACKSIZE 32768		/* tamanho de pilha das threads */

void dispatcherBody();
task_t *scheduler();

void ppos_init () {
	/* desativa o buffer da saida padrao (stdout), usado pela função printf */
	setvbuf (stdout, 0, _IONBF, 0) ;
	Current = &Main;
	Current->prev = NULL;
	Current->next = NULL;
	Current->id = 0;

	task_create(&Dispatcher, dispatcherBody, NULL);
}

int task_create (task_t *task, void (*start_func)(void *), void *arg) {
	char *stack;

	stack = malloc(STACKSIZE);

	getcontext (&task->context) ;

	stack = malloc (STACKSIZE) ;
	if (stack)
	{
		task->context.uc_stack.ss_sp = stack ;
		task->context.uc_stack.ss_size = STACKSIZE ;
		task->context.uc_stack.ss_flags = 0 ;
		task->context.uc_link = 0 ;
		makecontext (&task->context, (void*) (*start_func), 1, arg) ;
		task->id = ++id;
		#ifdef DEBUG
		printf ("task_create: criou tarefa %d\n", task->id) ;
		#endif

		queue_append((queue_t **) &ready, (queue_t *) task);
		if (task->id != 1){ // se nao for o dispatcher
			userTasks++;
		}
	}
	else
	{
		perror ("Erro na criação da pilha: ") ;
		exit (1) ;
	}
	

	return(task->id);
}

void task_exit (int exitCode) {
	#ifdef DEBUG
	printf ("task_exit: tarefa %d sendo encerrada \n", task_id()) ;
	#endif
	if (Current->id == 1)
		task_switch(&Main);
	else {
		queue_remove((queue_t **)&ready, (queue_t *) ready->prev);
		userTasks--;
		task_switch(&Dispatcher);
	}
}

int task_switch (task_t *task) {
	task_t *aux;

	aux = Current;
	Current = task;

	#ifdef DEBUG
	printf ("task_switch: trocando contexto %d -> %d\n", aux->id, task->id) ;
	#endif
	swapcontext(&aux->context, &task->context);

	if (errno >= 0)
		return(-1);


	return(0);
}

int task_id () {
	return (Current->id);
}

void task_yield () {
	task_switch(&Dispatcher);
}

task_t *scheduler(){
	ready = ready->next;
	return(ready);
}

void dispatcherBody() // dispatcher é uma tarefa
{
	task_t *next;
	// printf("%d\n", userTasks);
	while (userTasks > 0)
	{
	  next = scheduler() ;  // scheduler é uma função
	  if (next)
	  {
	     task_switch (next) ; // transfere controle para a tarefa "next"
	  }
	}
	task_exit(0) ; // encerra a tarefa dispatcher
}