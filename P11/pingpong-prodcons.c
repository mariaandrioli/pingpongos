// Maria Teresa Kravetz Andrioli GRR20171602
// Ana Carolina Faria Magnoni GRR20166808
#include <stdio.h>
#include <stdlib.h>
#include "ppos.h"

#define BUFSIZE 5

semaphore_t s_buffer, s_item, s_vaga;
task_t      p1, p2, p3, c1, c2;
int item;
int buffer[BUFSIZE];

void produtor(void *arg){
	while(1){
		int cont = 0;
		task_sleep(1000);
		item = random() % 100;

		sem_down(&s_vaga);
		sem_down(&s_buffer);

		for (int i = 0; i < BUFSIZE && cont <= 0; ++i)
			if (buffer[i] < 0){
				buffer[i] = item;
				cont = 1;
				printf ("%s produziu %d\n", (char *) arg, buffer[i]) ;
			}

		sem_up(&s_buffer);
		sem_up(&s_item);

	}
}
void consumidor(void *arg){
	while(1){
		int cont = 0;

		sem_down(&s_item);
		sem_down(&s_buffer);

		for (int i = 0; i < BUFSIZE && cont <= 0; ++i)
			if (buffer[i] >= 0){
				printf ("%s consumiu %d\n", (char *) arg, buffer[i]) ;
				buffer[i] = -1;
				cont = 1;
			}

		sem_up(&s_buffer);
		sem_up(&s_vaga);

		task_sleep(1000);
	}
}

int main(int argc, char *argv[])
{
	printf ("main: inicio\n") ;

    ppos_init () ;

	// cria semaforos
	sem_create (&s_buffer, 0) ;
	sem_create (&s_item, 0) ;
	sem_create (&s_vaga, BUFSIZE) ;

	for (int i = 0; i < BUFSIZE; ++i)
		buffer[i] = -1;

	// cria tarefas
	task_create (&p1, produtor, "p1") ;
	task_create (&p2, produtor, "p2") ;
	task_create (&p3, produtor, "p3") ;
	task_create (&c1, consumidor, "					c1") ;
	task_create (&c2, consumidor, "					c2") ;

	sem_destroy (&s_buffer) ;
	sem_destroy (&s_item) ;
	sem_destroy (&s_vaga) ;

	printf ("main: fim\n") ;
	task_exit (0) ;

	exit (0) ;

	return 0;
}
