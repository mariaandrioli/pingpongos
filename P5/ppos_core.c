// Maria Teresa Kravetz Andrioli GRR20171602
// Ana Carolina Faria Magnoni GRR20166808
#include "ppos.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/time.h>

task_t Main, *Current, Dispatcher;
int id, contaTicks;
int userTasks = 0;
task_t *ready;
int usando_prio = 0;

#define STACKSIZE 32768		/* tamanho de pilha das threads */

// estrutura que define um tratador de sinal (deve ser global ou static)
struct sigaction action ;

// estrutura de inicialização to timer
struct itimerval timer;

void dispatcherBody();
task_t *scheduler();

void tratador (int signum){
	if(!Current->sis_task){
		if(contaTicks > 0){
			contaTicks--;
		}
		else{
			task_yield();
		}
	}
}

void ppos_init () {
	/* desativa o buffer da saida padrao (stdout), usado pela função printf */
	setvbuf (stdout, 0, _IONBF, 0) ;
	contaTicks = 20;
	Current = &Main;
	Current->prev = NULL;
	Current->next = NULL;
	Current->id = 0;

	task_create(&Dispatcher, dispatcherBody, NULL);
	queue_remove((queue_t **)&ready, (queue_t *) ready);
	userTasks--;
	Dispatcher.sis_task = 1;

	action.sa_handler = tratador;
	sigemptyset(&action.sa_mask);
	action.sa_flags = 0;
	if (sigaction (SIGALRM, &action, 0) < 0)
	{
	  perror ("Erro em sigaction: ");
	  exit (1);
	}

	// ajusta valores do temporizador
	timer.it_value.tv_usec = 1000;      // primeiro disparo, em micro-segundos
	timer.it_value.tv_sec  = 0;      // primeiro disparo, em segundos
	timer.it_interval.tv_usec = 1000;   // disparos subsequentes, em micro-segundos
	timer.it_interval.tv_sec  = 0;   // disparos subsequentes, em segundos
	
	// arma o temporizador ITIMER_REAL (vide man setitimer)
	if (setitimer (ITIMER_REAL, &timer, 0) < 0)
	{
	perror ("Erro em setitimer: ");
	exit (1);
	}
}

int task_create (task_t *task, void (*start_func)(void *), void *arg) {
	char *stack;

	stack = malloc(STACKSIZE);

	getcontext (&task->context) ;

	stack = malloc (STACKSIZE) ;
	if (stack)
	{
		task->context.uc_stack.ss_sp = stack ;
		task->context.uc_stack.ss_size = STACKSIZE ;
		task->context.uc_stack.ss_flags = 0 ;
		task->context.uc_link = 0 ;
		makecontext (&task->context, (void*) (*start_func), 1, arg) ;
		task->id = ++id;
		if(task != &Dispatcher){
			task->sis_task = 0;
		}

		#ifdef DEBUG
		printf ("task_create: criou tarefa %d\n", task->id) ;
		#endif
		queue_append((queue_t **) &ready, (queue_t *) task);
		userTasks++;
	}
	else
	{
		perror ("Erro na criação da pilha: ") ;
		exit (1) ;
	}
	

	return(task->id);
}

void task_exit (int exitCode) {
	#ifdef DEBUG
	printf ("task_exit: tarefa %d sendo encerrada \n", task_id()) ;
	#endif
	if (Current->id == 1)
		task_switch(&Main);
	else {
		queue_remove((queue_t **)&ready, (queue_t *) Current);
		userTasks--;
		task_switch(&Dispatcher);
	}
}

int task_switch (task_t *task) {
	task_t *aux;

	aux = Current;
	Current = task;

	#ifdef DEBUG
	printf ("task_switch: trocando contexto %d -> %d\n", aux->id, task->id) ;
	#endif
	swapcontext(&aux->context, &task->context);

	if (errno >= 0)
		return(-1);

	return(0);
}

int task_id () {
	return (Current->id);
}

void task_yield () {
    #ifdef DEBUG
	printf("task_yield: a tarefa %d pediu yield\n", Current->id);
    #endif
    task_switch(&Dispatcher);
}

task_t *scheduler(){
	task_t *prox = ready;
    ready = ready->next;
    
    if(usando_prio){
	    task_t *aux = ready->next;
	    prox = ready;
	    for (int i = 0; i < userTasks; ++i)
	    {
	        if((aux->din_prio < prox->din_prio) || 
	        	((aux->din_prio == prox->din_prio) && 
	        		(aux->est_prio < prox->est_prio)))
	            prox = aux;
	        aux = aux->next;
	    }

	    aux = ready->next;
	    
	    for (int i = 0; i < userTasks; ++i)
	    {
	        if(aux->din_prio > -20){
	            aux->din_prio--;
	            #ifdef DEBUG
	            printf("scheduler: a tarefa %d tem prio_din %d\n", aux->id, aux->din_prio);
	            #endif
	        }
	        aux = aux->next;
	    }
	    prox->din_prio = prox->est_prio;
    }

    return(prox);
}

void dispatcherBody() { // dispatcher é uma tarefa 
	task_t *next;
	// printf("%d\n", userTasks);
	while (userTasks > 0)
	{
	  next = scheduler() ;  // scheduler é uma função
	  if (next)
	  {
	  	contaTicks = 20;
	    task_switch(next) ; // transfere controle para a tarefa "next"
	  }
	}
	task_exit(0) ; // encerra a tarefa dispatcher
}

void task_setprio (task_t *task, int prio) {
	usando_prio = 1;
	if(task == NULL){
		Current->est_prio = prio;
		Current->din_prio = prio;
	}
	else{
		task->est_prio = prio;
		task->din_prio = prio;
	}
}

int task_getprio (task_t *task) {
	if(task == NULL)
		return(Current->est_prio);
	else
		return(task->est_prio);

}