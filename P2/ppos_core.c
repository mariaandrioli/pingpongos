// Maria Teresa Kravetz Andrioli GRR20171602
// Ana Carolina Faria Magnoni GRR20166808
#include "ppos.h"
#include <errno.h>

task_t Main, *Current;
int id;

#define STACKSIZE 32768		/* tamanho de pilha das threads */


void ppos_init () {
	/* desativa o buffer da saida padrao (stdout), usado pela função printf */
	setvbuf (stdout, 0, _IONBF, 0) ;
	Current = &Main;
	Current->prev = NULL;
	Current->next = NULL;
	Current->id = 0;
}

int task_create (task_t *task, void (*start_func)(void *), void *arg) {
	char *stack;

	stack = malloc(STACKSIZE);

	getcontext (&task->context) ;

	stack = malloc (STACKSIZE) ;
	if (stack)
	{
		task->context.uc_stack.ss_sp = stack ;
		task->context.uc_stack.ss_size = STACKSIZE ;
		task->context.uc_stack.ss_flags = 0 ;
		task->context.uc_link = 0 ;
		makecontext (&task->context, (void*) (*start_func), 1, arg) ;
		task->id = ++id;
		#ifdef DEBUG
		printf ("task_create: criou tarefa %d\n", task->id) ;
		#endif
	}
	else
	{
		perror ("Erro na criação da pilha: ") ;
		exit (1) ;
	}
	

	return(task->id);
}

void task_exit (int exitCode) {
	#ifdef DEBUG
	printf ("task_exit: tarefa %d sendo encerrada \n", task_id()) ;
	#endif
	task_switch(&Main);
}

int task_switch (task_t *task) {
	task_t *aux;

	aux = Current;
	Current = task;

	#ifdef DEBUG
	printf ("task_switch: trocando contexto %d -> %d\n", aux->id, task->id) ;
	#endif
	swapcontext(&aux->context, &task->context);

	if (errno >= 0)
		return(-1);


	return(0);
}

int task_id () {
	return (Current->id);
}
